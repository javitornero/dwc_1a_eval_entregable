///////////////////////////////////////////////////////////////////
//FUNCIONES AUXILIARES PARA LA CONVERSIÓN
var masterBin=['0000','0001','0010','0011','0100','0101','0110','0111','1000','1001','1010','1011','1100','1101','1110','1111','10000','10001','10010','10011','10100','10101','10110','10111','11000','11001','11010','11011','11100','11101','11110','11111','100000','100001'];
var masterAlfaNum=['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','Z'];

function desdeHexToBin(_num){ // De hex->dec
	var i, j;
	var numBin=[];
	var arrayNum=[]=_num.split(''); // Array con los dígitos del número
	for(i=0;i<arrayNum.length;i++){
		for(j=0;j<masterAlfaNum.length;j++){
			if (arrayNum[i]==masterAlfaNum[j]) {
				numBin.push(masterBin[j]);
			}
		}
	}
	return numBin.join(''); // Devuelve número en binario
}
function desdeBinToHex(_num){ // De bin->hex
	var i, j;
	var ini=0,fin=4;
	var auxDig=[], numHex=[];
	var arrayNum=[]=_num.split(''); // Array con los dígitos del número
	var g4=arrayNum.length/4; // Dividimos para saber cuantos grupos de 4 díg. tiene el binario
	var toFill4=4-(4*(g4-parseInt(g4))); // Cuantos díg. faltan para completar otro grupo de 4
	if (toFill4<4) { // Si faltan menos de 4 añadimos los ceros por la izquierda hasta completar
		for(i=0;i<toFill4;i++){
			arrayNum.unshift('0'); // Añadimos ceros por la izq
		}	
	}
	for(i=0;i<arrayNum.length/4;i++){ // Recorremos el binario
		auxDig[i]=arrayNum.slice(ini,fin).join(''); // Asignamos a una array los diferentes cuartetos
		for(j=0;j<masterBin.length;j++){ //Recorremos la masterBin[]
			if (auxDig[i]==masterBin[j]) { // Comparamos y si son iguales
				numHex.push(masterAlfaNum[j]); // Guardamos en numHex[] su equivalente en masterAlfaNum[]
			}
		}
		ini+=4; // Para la siguiente vuelta del bucle avanzamos 4 posiciones
		fin+=4; // tanto del inicio como del fin
	}
	return numHex.join(''); // Devuelve el número en hexadecimal
}
function desdeDecimal(_num,_baseO,_baseD){ // De dec->base destino 
	var cociente,residuo;
	var arrayResiduo=[];
	do{
		cociente=_num/_baseD;
		residuo=_num%_baseD;
		if (residuo>9) { // Si el residuo es mayor que 9..
			residuo=masterAlfaNum[residuo]; // Lo reemplazamos por la letra que le corresponda
		}
		arrayResiduo.unshift(residuo);
		_num=parseInt(cociente); // Asignamos parte entera
	}while(cociente>=1)
	return arrayResiduo.join(''); // Devuelve número en base destino indicada (excepto a hex)
}
function aDecimal(_num,_baseO){ // De base origen->dec
	var i, j;
	var numDec=0;
	var arrayNum=[]=_num.split('').reverse(); // Array con los dígitos del número y lo revertimos
	for(i=0;i<arrayNum.length;i++){
		if (arrayNum[i]>'9') { // Si es letra..
			for(j=0;j<masterAlfaNum.length;j++){ // recorremos el array que las contiene todas..
				if (arrayNum[i]==masterAlfaNum[j]) { // Si encuentra la coincidencia..
					arrayNum[i]=j; // asignamos la posición que ocupa al array que estamos rellenando para luego poder el cálculo de de su potencia correspondiente
				}
			}
		}
		numDec+=arrayNum[i]*Math.pow(_baseO,i);
	}
	return numDec; // Devuelve número en decimal
}
///////////////////////////////////////////////////////////////////
//SALIDA
function imprime(_num,_baseO,_baseD,_conv){
	document.getElementById('pSalida').innerHTML='La conversión del número '+_num+' en base origen '+_baseO+' a base destino '+_baseD+' es: '+_conv;
}
function imprimeError(_codError){
	var codError0='Hay campos vacios.';
	var codError1='No se contemplan fracciones en este conversor';
	var codError2='La base origen y destino es la misma.';
	var codError3='Este conversor no trabaja con la BASE ORIGEN indicada.<br>Bases admitidas: de la 2 a la 34.';
	var codError4='Este conversor no trabaja con la BASE DESTINO indicada.<br>Bases admitidas: de la 2 a la 34.';
	var codError5='El número no está en la base origen.';
	var arrayCodError=[codError0,codError1,codError2,codError3,codError4,codError5];
	document.getElementById('pSalida').innerHTML='Error: '+arrayCodError[_codError];
}
///////////////////////////////////////////////////////////////////
//CONTROL DE ERRORES
function excepciones(_num,_baseO,_baseD){
	if (checkVacios(_num,_baseO,_baseD)=='0'){
		return '0';
	}else if(checkFractions(_num)=='1') {
		return '1';
	}else if (checkBasesIguales(_baseO,_baseD)=='2') {
		return '2';
	}else if (checkBaseO(_baseO)=='3') {
		return '3';
	}else if (checkBaseD(_baseD)=='4') {
		return '4';
	}else if (checkNumBelongsToBaseO(_num,_baseO)=='5') {
		return '5';
	}
	return 'OK';
	// Si devuelve OK es que todas las funciones check han devuelto OK = número válido
}
function checkVacios(_num,_baseO,_baseD){
	if (_num=='' || _baseO=='' || _baseD=='') {
		return '0';
	}
	return 'OK';
}
function checkFractions(_num){
	var coma=_num.indexOf(','); // Si la función indexOf no encuentra coincidencias
	var punto=_num.indexOf('.'); // coma y punto valdrán -1
	if (coma!=-1 || punto!=-1) { // Si valen -1
		return '1';	// Devolvemos el código de error
	}
	return 'OK';
}
function checkBasesIguales(_baseO,_baseD){
	if (_baseO==_baseD) {
		return '2';	
	}
	return 'OK';
}
function checkBaseO(_baseO){
	var baseO=parseInt(_baseO);
	if(baseO>1 && baseO<35){
		return 'OK';
	}
	return '3';
}
function checkBaseD(_baseD){
	var baseD=parseInt(_baseD);
	if(baseD>1 && baseD<35){
		return 'OK';
	}
	return '4';
}
function checkNumBelongsToBaseO(_num,_baseO){
	var i, j;
	var neg=false;
	if (_num.substring(0,1)=='-') { // Controla si es número negativo
		_num=_num.substring(1);
		neg=true;
	}
	var arrayNum=[]=_num.split(''); // Array con los dígitos del número
	for(i=0;i<arrayNum.length;i++){
		var flag='5';
		for(j=0;j<parseInt(_baseO);j++){
			if (arrayNum[i]==masterAlfaNum[j]) {
				flag='OK';
			}
		}
		if (flag=='5') {
			return flag;
		}
	}
	return flag;
}
///////////////////////////////////////////////////////////////////
//CONTROLADOR GENERAL
function manejador(e){
	var num=document.getElementById('textNum').value;
	var neg=false; // Inicialiazación de bandera
	if (num.toString().substring(0,1)=='-') { // Controla si es número negativo
		num=num.substring(1); // Le quitamos el signo de negativo para trabajar con él
		neg=true;  // Es negativo
	}
	num=num.toUpperCase(); // Si hay letras las pasamos a mayúsculas
	var baseO=document.getElementById('textBaseO').value;
	var baseD=document.getElementById('textBaseD').value;
	var conv; // Número resultado de la conversión
	var codError=excepciones(num,baseO,baseD);
	if (codError!='OK') {
		imprimeError(codError); // Manejo de excepciones controlando de que tipo de error se trata
	}else{
		if (baseD==16) { // Si la base destino es hex
			if (baseO==2) {
				conv=desdeBinToHex(num); // De bin->hex
			}else if (baseO==10) {
				conv=desdeBinToHex(desdeDecimal(num,10,2)); // De dec->bin->hex
			}else{
				conv=desdeBinToHex(desdeDecimal(aDecimal(num,baseO),baseO,2));
				// De base origen->dec->bin->hex
			}
		}else if (baseO==16) { // Si la base origen es hex
			if (baseD==2) {
				conv=desdeHexToBin(num); // De hex->bin
			}else if (baseD==10) {
				conv=aDecimal(desdeHexToBin(num),2); // De hex->bin->dec
			}else{
				conv=desdeDecimal(aDecimal(desdeHexToBin(num),2),baseO,baseD);
				// De hex->bin->dec->base destino
			}
		}else{
			if (baseO==10) {
				conv=desdeDecimal(num,baseO,baseD); // De dec->base destino excepto hex
			}else{
				if (baseO==2 && baseD==10) { // De bin->base dec
					conv=aDecimal(num,baseO);
				}else{
					conv=desdeDecimal(aDecimal(num,baseO),baseO,baseD);
					// Desde cualquier base que no sea hex, dec o bin a cualquier base destino que no sea hex o dec
				}
			}
		}
		if (neg==true) { // Si el número dado es negativo añadimos el signo negativo
			var arrayConv=[]=conv.split(''); // Para hacerlo trataremos el numero como un array
			arrayConv.unshift('-'); // Añadimos el signo neg por la izquierda
			conv=arrayConv.join(''); // Lo volvemos a pasar a string
		}
		imprime(num,baseO,baseD,conv); // Pasamos las variables que queremos imprimir
	} // Empiezo aqui
}
///////////////////////////////////////////////////////////////////
window.onload=function(){	
	document.getElementById('buttonConvert').onclick=manejador; //LLamada al manejador de eventos
}