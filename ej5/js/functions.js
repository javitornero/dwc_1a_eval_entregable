var arrayOp1=[];
var arrayOp2=[];
var fun='', resultado='';
var contComa=0, contNeg=0;
///////////////////////////////////////////////////////////////////
//FUNCIONES AUXILIARES, DE CONTROL DE ERRORES Y DE SALIDA
function del(){ // Elimina elemento del array por la parte del final
	if (fun=='') { // Si la var fun está vacía el se aplica al operador1
		arrayOp1.pop();
		document.getElementById('tdOp1').innerHTML=arrayOp1.join('');
	}else{ // Si la var fun NO está vacía el se aplica al operador2
		arrayOp2.pop();
		document.getElementById('tdOp2').innerHTML=arrayOp2.join('');
	}
}
function añadeDigito(_digito){
	if (fun=='') { // Si la var fun está vacía el dígito se guarda en arrayOp1
		añadeDigitoAux(arrayOp1,_digito,'tdOp1');
	}else if (resultado!='' || resultado=='0' ) { // Si se introduce un dígito posterior a una operación
		reset(); // Se resetea todo
		añadeDigitoAux(arrayOp1,_digito,'tdOp1');
	}else{ // Si existe fun pero no resultado es que falta por indicar el operador2, el dígito se introduce entonces en arrayOp2 
		añadeDigitoAux(arrayOp2,_digito,'tdOp2');
	}
}
function añadeDigitoAux(_arrayOp,_digito,_tdOp){
	if (_digito==',' && _arrayOp.length==0) { // si se introduce como primer dígito la coma
		_arrayOp.push('0'+_digito); // Introduce un cero a la izq. de la coma
		document.getElementById(_tdOp).innerHTML=_arrayOp.join('');
	}else{ // Si no es el primero 
		_arrayOp.push(_digito); // Introduce dígito
		document.getElementById(_tdOp).innerHTML=_arrayOp.join('');
	}
}
function añadeFun(_fun){
	if (fun=='') { 
		if(arrayOp1.length==0){ // Si se pide una operación sin operador1
			añadeDigito('0'); // Operador1 será cero
		};
		fun=_fun;
		document.getElementById('tdFun').innerHTML=fun;
		contComa=0;
		contNeg=0;
	};
}
function añadeComa(_contComa){
	if (_contComa==0) { // Si el contador de comas es diferente a 0 no permitirá añadir mas comas
		añadeDigito(',');
	};
	contComa++;
}
function añadeNeg(_contComa){
	if (contNeg==0) { // Si el contador de signos neg. es diferente a 0 no permitirá añadir mas
		if (fun=='') { // Si fun está vacio, lo introduce en arrayOp1
			arrayOp1.unshift('-'); // lo introduce por la izq.
			document.getElementById('tdOp1').innerHTML=arrayOp1.join('');
		}else if (resultado!='') { // Si se introduce posterior a una operación
			reset(); // Te resetea todo
			arrayOp1.unshift('-'); // Se lo pasa a arrayOp1
			document.getElementById('tdOp1').innerHTML=arrayOp1.join('');
		}else{
			arrayOp2.unshift('-'); // Si existe fun pero no resultado se lo pasa a operador2, el símbolo se introduce entonces en arrayOp2 
			document.getElementById('tdOp2').innerHTML=arrayOp2.join('');
		}
	};
	contNeg++;
}
function calcula(_fun,_op1,_op2){
	_op1=_op1.replace(',','.'); // Se sustituyen las comas por los puntos para evitar confictos en las operaciones con floats
	_op2=_op2.replace(',','.');
	switch (_fun) { // Dependiendo del valor de _fun hará una opración u otra 
	    case '+':
	        resultado = parseFloat(_op1)+parseFloat(_op2); // Se requiere parsear a float para que se sumen y no se concatenen los valores
	        break;
	    case '-':
	        resultado = _op1-_op2;
	        break;
	    case '÷':
		    if (_op2==0) { // División entre cero no permitido
		    	resultado='No se puede dividir entre cero';
		    	break;
		    }else{
	        	resultado = _op1/_op2;
	        	break;
	    	}
	    case 'x':
	        resultado = _op1*_op2;
	        break;
	    case '√':
	    	if (_op1>=0) {
		        resultado = Math.sqrt(_op1);
		        break;
		    }else{ // Raices negativas no permitidas
		    	resultado='Entrada no válida';
		    	break;
		    }
	    case '^':
	        resultado = Math.pow(_op1,_op2);
	        break;
	}
	document.getElementById('tdRes').innerHTML=resultado.toString().replace('.',','); // Se convierte a string y sustituimos punto por coma para mostrarlo
	contComa=0;
	contNeg=0;
}
function reset(){ // Función que resetea variables, arrays y aquellos elementos del DOM de los que nos valemos para imprimir la salida
	arrayOp1=[];
	arrayOp2=[];
	fun='',resultado='';
	contNeg=0, contComa=0;
	document.getElementById('tdOp1').innerHTML=arrayOp1.join('');
	document.getElementById('tdOp2').innerHTML=arrayOp2.join('');
	document.getElementById('tdRes').innerHTML='';
	document.getElementById('tdFun').innerHTML='';
}
///////////////////////////////////////////////////////////////////
//CONTROLADORES
function clickButtonReset(e){
	reset();
}
function clickButtonDel(e){
	del();
}
function clickButtonPow(e){
	añadeFun('^');
}
function clickButtonRaiz(e){
	añadeFun('√');
	calcula(fun, arrayOp1.join(''), arrayOp2.join(''));
}
function clickButtonDiv(e){
	añadeFun('÷');
}
function clickButton7(e){
	añadeDigito('7');
}
function clickButton8(e){
	añadeDigito('8');
}
function clickButton9(e){
	añadeDigito('9');
}
function clickButtonMulti(e){
	añadeFun('x');
}
function clickButton4(e){
	añadeDigito('4');
}
function clickButton5(e){
	añadeDigito('5');
}
function clickButton6(e){
	añadeDigito('6');
}
function clickButtonRes(e){
	añadeFun('-');
}
function clickButton1(e){
	añadeDigito('1');
}
function clickButton2(e){
	añadeDigito('2');
}
function clickButton3(e){
	añadeDigito('3');
}
function clickButtonSum(e){
	añadeFun('+');
}
function clickButtonNeg(e){
	añadeNeg(contNeg);
}
function clickButton0(e){
	añadeDigito('0');
}
function clickButtonComa(e){
	añadeComa(contComa);
}
function clickButtonIgual(e){
	calcula(fun, arrayOp1.join(''), arrayOp2.join(''));
}
///////////////////////////////////////////////////////////////////////////////////////////////
window.onload=function(){	
	document.getElementById('buttonReset').onclick=clickButtonReset;
	document.getElementById('buttonDel').onclick=clickButtonDel;
	document.getElementById('buttonPow').onclick=clickButtonPow;
	document.getElementById('buttonRaiz').onclick=clickButtonRaiz;
	document.getElementById('buttonDiv').onclick=clickButtonDiv;
	document.getElementById('button7').onclick=clickButton7;
	document.getElementById('button8').onclick=clickButton8;
	document.getElementById('button9').onclick=clickButton9;
	document.getElementById('buttonMulti').onclick=clickButtonMulti;
	document.getElementById('button4').onclick=clickButton4;
	document.getElementById('button5').onclick=clickButton5;
	document.getElementById('button6').onclick=clickButton6;
	document.getElementById('buttonRes').onclick=clickButtonRes;
	document.getElementById('button1').onclick=clickButton1;
	document.getElementById('button2').onclick=clickButton2;
	document.getElementById('button3').onclick=clickButton3;
	document.getElementById('buttonSum').onclick=clickButtonSum;
	document.getElementById('buttonNeg').onclick=clickButtonNeg;
	document.getElementById('button0').onclick=clickButton0;
	document.getElementById('buttonComa').onclick=clickButtonComa;
	document.getElementById('buttonIgual').onclick=clickButtonIgual;
}