///////////////////////////////////////////////////////////////////
//FUNCIONES AUXILIARES
var arrayLista=[];
var arrayReg=[];
function anyade(_nombre,_peso,_altura){ // Mete el nuevo registro en array
	arrayLista.push(arrayReg=[_nombre,_peso,_altura]);
}
///////////////////////////////////////////////////////////////////
//SALIDA
function imprimeTabla(){
	var i, j;
	var tabla=document.getElementById('idTabla'); // Tabla existente en nuestro HTML
	for(i=arrayLista.length-1;i<arrayLista.length;i++){		
    	var fila = document.createElement("tr");//Asigna en var fila nuevo elemento tr
		for(j=0;j<arrayReg.length;j++){
			var celda = document.createElement("td"); //Asigna en var celda nuevo elemento td
	      	var textoCelda = document.createTextNode(arrayLista[i][j]); // Asigna a la var textoCrea un texto
	      	celda.appendChild(textoCelda); // Asigna a celda el textoCelda
  			celda.setAttribute("id", ""+i+j); // Identificamos cada td para luego poder referenciarlos
  			celda.style.backgroundColor="silver"; // Un poco de estilo css a la celda
	      	fila.appendChild(celda); // Asigna a fila la celda
	      	tabla.appendChild(fila); // asigna a la tabla la fila
		}
		var tdPeso=document.getElementById(''+i+1).textContent;
		var tdAltura=document.getElementById(''+i+2).textContent;
		if (parseInt(tdPeso)>(parseInt(tdAltura)-100)) { // Si el peso es superior a la altura -100
			document.getElementById(''+i+'0').style.color='red'; // Pinta el nombre en rojo
		}else{ // Si no es superior
			document.getElementById(''+i+'0').style.color='blue'; // Lo pinta en azul
		}
	}
}

function imprimeError(_codError){
	var codError0='Hay campos vacios.';
	var codError1='Los campos peso y altura deben ser números';
	var codError2='Los campos peso y altura deben ser números positivos';
	var arrayCodError=[codError0,codError1,codError2];
	alert('Error: '+arrayCodError[_codError]);
}
///////////////////////////////////////////////////////////////////
//CONTROL DE ERRORES
function excepciones(_nombre,_peso,_altura){
	if (checVacios(_nombre,_peso,_altura)=='0') {
		return '0';
	}else if (checkEsNum(_peso,_altura)=='1') {
		return '1';
	}else if (checkEsNumPos(_peso,_altura)=='2') {
		return '2';
	}
	return 'OK';
}
function checVacios(_nombre,_peso,_altura){
	if (_nombre=='' || _peso=='' || _altura=='') {
		return '0';
	};
	return 'OK';
}
function checkEsNum(_peso,_altura){
	if (isNaN(_peso) || isNaN(_altura)) {
		return '1';
	};
	return 'OK';
}
function checkEsNumPos(_peso,_altura){
	if (parseInt(_peso)<=0 || parseInt(_altura)<=0) {
		return '2';
	};
	return 'OK';
}
///////////////////////////////////////////////////////////////////
//CONTROLADOR GENERAL
function manejador(e){
	var nombre=document.getElementById('textNombre').value;
	var peso=document.getElementById('textPeso').value;
	var altura=document.getElementById('textAltura').value;
	var codError=excepciones(nombre,peso,altura);
	if (codError!='OK') { // Manejo de excepciones
		imprimeError(codError);
	}else{ // Si el código de error es OK entonces..
		anyade(nombre,peso,altura); // añade el una array con estas tres variables como elemento de otra array
		imprimeTabla(); // y añade los nuevos elementos HTML donde se imprimira el nuevo registro y lo imprime
	}
}
///////////////////////////////////////////////////////////////////
window.onload=function(){
	document.getElementById('buttonAnyadir').onclick=manejador; // LLamada al manejador de eventos
}