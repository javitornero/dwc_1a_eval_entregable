///////////////////////////////////////////////////////////////////
//FUNCIONES AUXILIARES
function reset(){
	pSalida.innerHTML='';
	pSalidaAux.innerHTML='';
}
function tirada(){
	var counter=0, uno=0, dos=0, tres=0, cuatro=0, cinco=0, seis=0, veces=0, maxOcur=0;
	var arrayValores=[], arrayRanking=[], arrayResults=[], arrayEmpate=[];
	var intervalo=setInterval(
		function(){
			arrayValores[counter]=Math.floor((Math.random()*6)+1); // Rellenamos array con ocurrencias al azar
			document.images['imgDado'].src='img/dado'+arrayValores[counter]+'.png'; // Mostramos la imagen de la cara que corresponde al valor aleatorio obtenido
			setTimeout("document.images['imgDado'].src='img/dado0.png';",100);
			// Para conseguir alternancia entre las imágenes de las caras se me ha ocurrido en la línea de arriba meter una imagen en blanco para que el ojo pueda discernir entre una imagen y otra
		    counter++;
		    if(counter === 20) { // Cuando este contador llegue a 20..
		        clearInterval(intervalo); // Paramos la ejecución de la funcion setInterval()
		        for(i=0;i<arrayValores.length;i++){ // Recorremos el array con las ocurrencias
		        	switch (arrayValores[i]) { // Dependiendo del valor de arrayValores[i] hará una operación u otra 
					    case 1:
					        uno++;
					        break;
					    case 2:
					        dos++;
					        break;
					    case 3:
						    tres++;
						    break;
					    case 4:
					        cuatro++;
					        break;
					    case 5:
						    cinco++;
						    break;
					    case 6:
					    	seis++;
					        break;
					}
					arrayRanking=[uno,dos,tres,cuatro,cinco,seis]; // En cada pasada del for se actualizan los contadores
					arrayEmpate=[]; // Se inicializa el array
					var i, j;
					for(j=0;j<arrayRanking.length;j++){ // Se recorre ela array de los contadores
						if (arrayRanking[j]==veces) { // Si el valor del contador de la posición actual iguala el valor del contador mas repetido hasta el momento
							arrayEmpate.push(j+1); // Se añade el contador a un array de empatados
						}
						if(arrayRanking[j]>veces){ // Si el valor del contador de la posición actual es mayor que el valor del contador mas repetido hasta el momento
							veces=arrayRanking[j]; // Asignamos el valor del contador actual a la variable veces
							maxOcur=j+1; // Guardamos en la variable maxOcur el número que se corresponde con la posición actual +1 como ocurrencia mas repetida
						}
					}
		        }
				if (arrayEmpate.length>1) { // Si el arrayEmpate tiene mas de un elemento hay empate
					maxOcur=arrayEmpate.join(', '); // asignamos un string a maxOcur de aquellas ocurrencias que han empatado como mas repetidas
				}
		        imprime(uno,dos,tres,cuatro,cinco,seis,maxOcur,veces);
		    }
		},
	1000); // Un segundo por tirada, terminará cuando el contador alcance 20
}
///////////////////////////////////////////////////////////////////
//FUNCIONES SALIDA
function imprime(uno,dos,tres,cuatro,cinco,seis,maxOcur,veces){
	pSalida.innerHTML='La tirada mas repetida ha sido el número '+maxOcur+', se ha repetido '+veces+' veces.';
	pSalidaAux.innerHTML='Ocurrencia 1: '+uno+' veces<br>Ocurrencia 2: '+dos+' veces<br>Ocurrencia 3: '+tres+' veces<br>Ocurrencia 4: '+cuatro+' veces<br>Ocurrencia 5: '+cinco+' veces<br>Ocurrencia 6: '+seis+' veces';
}
///////////////////////////////////////////////////////////////////
//CONTROLADOR GENERAL
function manejador(e){
	reset();
	tirada();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
window.onload=function(){
	document.getElementById('buttonLanzar').onclick=manejador; // LLamada al manejador de eventos
}