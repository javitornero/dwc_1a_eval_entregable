var arrayE=['Elige equipo','Valencia','Barcelona','Madrid','Sevilla'];
var vacio=[];
var valencia=['Albes','Garay','Parejo','Munir','Nani'];
var barcelona=['Ter','Pique','Busquets','Iniesta','Mesi'];
var madrid=['Casilla','Ramos','Cros','Ronaldo','Benzema'];
var sevilla=['Vitolo','Banega','Gameiro','Luis','Alberto'];
var arrayJug=[vacio,valencia,barcelona,madrid,sevilla];
function pintaSelectE(){ // Pinta el select de equipos basándose en el arrayE
	var i;
	for(i=0;i<arrayE.length;i++){
		if (i==0) {
			var selectE=document.getElementById('selectE');
			var option=document.createElement('option');
			var texto=document.createTextNode(arrayE[i]);
			option.appendChild(texto);
			selectE.appendChild(option);
			selectE.options.selectedIndex=1;
			//document.write('<option value="'+i+'" selected="selected">'+arrayE[i]+'</option>');
		}else{
			var selectE=document.getElementById('selectE');
			var option=document.createElement('option');
			var texto=document.createTextNode(arrayE[i]);
			option.appendChild(texto);
			selectE.appendChild(option);
			//document.write('<option value="'+i+'">'+arrayE[i]+'</option>');
		}
	}
}
function recargaSelectJug(_pos){
	var i;
	var selectJug=document.getElementById("selectJug");
	while (selectJug.firstChild) { // Vacia el select de jugadores
   		selectJug.removeChild(selectJug.firstChild)
	}
	for(i=0;i<arrayJug[_pos].length;i++){ // Llena el select de jugadores dependiendo del equipo seleccionado
		var option=document.createElement('option');
		var texto=document.createTextNode(arrayJug[_pos][i]);
		option.appendChild(texto);
		selectJug.appendChild(option);
	}
}
function manejador(e){ // Esccucha cuando se produce un cambio en una de las opciones del select de equipos
	var pos=document.getElementById('selectE').options.selectedIndex;
	recargaSelectJug(pos);
}
window.onload=function(){
	pintaSelectE(); // Cargamos el select de equipos dinámicamente
	document.getElementById('selectE').onchange=manejador; //LLamada al manejador de eventos del select de equipos
}